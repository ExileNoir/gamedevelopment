package BlackjackGame;

import java.util.Scanner;


public class GameLogic {

	// Instance Variables
	private  boolean endRound = false;
	private  int playerMoney = 0;
	private  int playerBet = 0;
	private  int response = 0;      // to see if player wants to hit or stand



	// User Input
	private  int getUserInput(String message) {
		Scanner userInput = new Scanner(System.in);
		System.out.println(message);
		return userInput.nextInt();
	}

	// Playing Deck - create and shuffle
	private  Deck playingDeck() {
		Deck playingDeck = new Deck();
		playingDeck.createFullDeck();
		playingDeck.shuffleDeck();
		return playingDeck;
	}

	// Input Money For Gambling
	public  int gambleMoneyInput() {
		playerMoney = getUserInput("We have a Player " +
				"\nPut your gambling money on the table BABY!");
		System.out.println("******************************************************");
		while( playerMoney < 10_000){
			System.out.println("You cheap Fat Pussy!!");
			playerMoney = getUserInput("Nothing less then 10 000 or you're not coming into my Casino!");
			System.out.println("******************************************************");
		}
		return playerMoney;
	}

	// Input Money To Bet
	private  int takeBet() {
		// Take The Players Bet
		System.out.println("You have € " + playerMoney + ", how much would you like to bet?");
		playerBet = getUserInput("Place your bet!");
		System.out.println("******************************************************");
		while(playerBet < 5_000){
			System.out.println("As useful as a chocolate teapot!!\nNow, put a real bet on the table");
			playerBet = getUserInput("No bets under 5_000, you Wanker!");
			System.out.println("******************************************************");
		}
		return playerBet;
	}

	// Dealing Cards
	private  void startPlayerAndDealerDealCard(Deck playerCards, Deck dealerCards){
		System.out.println("Dealing...");
		playerCards.dealCard(playingDeck());        // Player gets 2 cards
		playerCards.dealCard(playingDeck());
		dealerCards.dealCard(playingDeck());       // Dealer gets 2 cards
		dealerCards.dealCard(playingDeck());
	}
	private  void playerDealCard(Deck playerCards){       // Player wants 1 card
		playerCards.dealCard(playingDeck());
	}
	private  void dealerDealCard(Deck dealerCards){     // Dealer wants 1 card
		dealerCards.dealCard(playingDeck());
	}

	// While Loop for Drawing new cards
	private  void displayPlayerDealerCardsAndValue(Deck playerCards, Deck dealerCards){
		// While Loop for Drawing new cards
		while (!endRound) {
			// Display Player Cards
			System.out.println("Your hand: " + playerCards.toString());
			// Display Value
			System.out.println("******************************************************");
			System.out.println("Your hand is currently valued at: " + playerCards.getTotal());
			// Display Dealer's cards
			System.out.println("Dealer Hand: " + dealerCards.getCard(0).toString() + " and [hidden card]");

			// What does player want to do now
			System.out.println();
			System.out.println("Would you like to: \n(1)HIT 'and be a man' " +
					"\nOR \n(2)STAND 'and be a PussyBoy");
			response = getUserInput("Take a chance love");
			// Player HIT's
			if (response == 1) {
				playerDealCard(playerCards);
				System.out.println("You draw a: " + playerCards.getCard(playerCards.deckSize() - 1).toString());
				// BUST if over 21
				if (playerCards.getTotal() > 21) {
					System.out.println("BUST!! Currently valued at: " + playerCards.getTotal());
					System.out.println("******************************************************");
					playerMoney -= playerBet;
					endRound = true;
					break;
				}
			}
			// Player STAND's
			if (response == 2) {
				break;
			}
		}
	}


	// See if Dealer has more points then Player
	private  void seeIfDealerHasMorePoitsThenPlayer(Deck playerCards, Deck dealerCards){
		while ((dealerCards.getTotal() > playerCards.getTotal()) && endRound == false) {
			System.out.println("******************************************************");
			System.out.println("Dealer beats you " + dealerCards.getTotal() + " to " + playerCards.getTotal());
			System.out.println("YOU LOST This hand");
			playerMoney -= playerBet;
			endRound = true;
		}
	}

	// Determine when Dealer Stands or Hits
	private  void dealerConditionOnHitOrStand(Deck dealerCards){
		// Display value of Dealer
		System.out.println("Dealers hand value: " + dealerCards.getTotal());
		// Dealer HITs at 17 && STANDs at 18
		while ((dealerCards.getTotal() < 17) && endRound == false) {
			dealerDealCard(dealerCards);
			System.out.println("Dealer draws: " + dealerCards.getCard(dealerCards.deckSize() - 1).toString());
		}
	}

	// Determine if Dealer is Busted
	private  void dealerBustedOrNot(Deck dealerCards){
		if ((dealerCards.getTotal() > 21) && endRound == false) {
			System.out.println("******************************************************");
			System.out.println("Dealer is Busted. YOU WIN! ");
			playerMoney += playerBet;
			endRound = true;
		}
	}

	// Determine if Push
	private  void pushPlayerVsDealer(Deck dealerCards, Deck playerCards){
		if ((dealerCards.getTotal() == playerCards.getTotal()) && endRound == false) {
			System.out.println("******************************************************");
			System.out.println("Push!");
			endRound = true;
		}
	}

	// Determine if Player Wins
	private  void determineIfPlayerWins(Deck playerCards, Deck dealerCards){
		if ((playerCards.getTotal() > dealerCards.getTotal()) && endRound == false) {
			System.out.println("******************************************************");
			System.out.println("You WIN the hand! "+playerCards.getTotal()+ " to "+dealerCards.getTotal());
			playerMoney += playerBet;
			endRound = true;
		} else if (endRound == false) {
			System.out.println("******************************************************");
			System.out.println("Dealer wins: "+dealerCards.getTotal()+ " to "+playerCards.getTotal());
			playerMoney -= playerBet;
		}
	}

	// End of Hand - Put Cards back in Deck
	private  void endOfHand(Deck playerCards, Deck dealerCards){
		playerCards.moveAllToDeck(playingDeck());
		dealerCards.moveAllToDeck(playingDeck());
		System.out.println("End of Hand!");
		System.out.println("******************************************************");
	}

	// Game Progress - Conditions
	public   void gameProgress(Deck playerCards, Deck dealerCards) {
		System.out.println("*****************************************************");
		System.out.println("**           Welcome to Blackjack                  **");
		System.out.println("**  It doesn't matter if you're black or white...  **");
		System.out.println("**  the only color that really matters is green!   **");
		System.out.println("** Where Casino Noir gladly takes all your MONEY!! **");
		System.out.println("*****************************************************");
		System.out.println("\n");

		gambleMoneyInput();

		while (playerMoney > 0) {
			endRound = false;                                // RESET ROUND TO FALSE     // (memento) little F***
			takeBet();
			// Break if the Player bets more then his Gambling Money
			if (playerBet > playerMoney) {
				System.out.println("CHEATER you little fat F*** CHEATER " +
						"\nYou have been kicked out Noir's Royal Casino");
				break;
			}
			// Start Game 2 Cards Each
			startPlayerAndDealerDealCard(playerCards, dealerCards);

			// While loop for drawing new cards
			displayPlayerDealerCardsAndValue(playerCards, dealerCards);

			// Dealer Reveals Cards
			System.out.println("******************************************************");
			System.out.println("Dealer Cards: " + dealerCards.toString());
			System.out.println("******************************************************");

			// See if dealer has more points then player
			seeIfDealerHasMorePoitsThenPlayer(playerCards, dealerCards);

			// condition for stand or not
			dealerConditionOnHitOrStand(dealerCards);


			// Display value of Dealer


			dealerBustedOrNot(dealerCards);

			pushPlayerVsDealer(dealerCards, playerCards);

			determineIfPlayerWins(playerCards, dealerCards);

			endOfHand(playerCards, dealerCards);
		}
		System.out.println("\n");
		System.out.println("******************************************************");
		System.out.println("**                  GAME OVER!                      **");
		System.out.println("**             You are out of money!                **\n**  Casino Noir Royal thanks you for your money     **\n**       Thank you and come again soon...           **");
		System.out.println("******************************************************");
		System.out.println("\n");
		System.out.println("May the force be with you productions...");
	}

}

/*
*
*       GAME LOGIC OVERVIEW  (Game Progress)
*
*       - Gamble Money Input
*           => while EndRound = false
*       - Take Bet
*       - Start Player And Dealer Deal Cards == 2 cards each
*       - Display Player & Dealer Cards And Values == while loop for drawing new cards
*          -> Dealer reveals his cards
*       - See If Dealer Has More Points Then Player
*       - Dealer Condition On Hit Or Stands
*           -> Display value of dealer
*       - Dealer Busted Or Not
*       - Push Player Vs Dealer
*       - Determine If Player Wins
*       - End Of Hand
*
*
* */
