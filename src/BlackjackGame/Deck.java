package BlackjackGame;

import java.util.ArrayList;
import java.util.Random;

public class Deck {

    // Instance Var
    public ArrayList<Card> cards;                                     // ArrayList of the Card (== Data inside will be changeable)
    Random rand = new Random();                                      // For Shuffle Deck

    // Constructor
    public Deck() {
        this.cards = new ArrayList<Card>();                        // 'cards' = ArrayList of Card
    }

    // Methods
        // CREATE 52 cards to playing deck
    public void createFullDeck(){                                      // 4(suits) * 13(ranks) = 52 full deck of cards
        for (Suit cardSuit : Suit.values()){                          // Generate CardsSuit / value method returns all 4  suits of Suits
            for (Rank cardRank : Rank.values()){                     // Generate CardRank / value method returns all 13 values of Rank
                this.cards.add(new Card(cardSuit, cardRank));       // 'add' a new card(suit+rank) to the deck (add to end of the ArrayList)
            }
        }
    }

        // SHUFFLE Cards
    public void shuffleDeck(){
        int pick = 0;
        for (int i = cards.size() - 1; i > 0; i--){          // 'i' = number of elements in ArrayList cards - 1 (end to start)
            pick = rand.nextInt(i);                         // 'pick' = random number of index location for elements in ArrayList cards - 1 (end to start)
            Card randCard = cards.get(pick);               // get element(item) at index 'pick'(random index location)
            Card lastCard = cards.get(i);                 // get element at index 'i'
            cards.set(i, randCard);                      // 'set' = replaces element at specified position (i / pick) in cards list with specified element (randCard / lastCard)
            cards.set(pick, lastCard);
        }
    }

        // REMOVE Card from the deck
    public void removeCard(int card){
        this.cards.remove(card);
    }

        // GET Card from the deck
    public Card getCard(int card){
        return this.cards.get(card);          // 'get' element/item at index(card) of ArrayList
    }

        // ADD Card to the deck
    public void addCard(Card addCard){
        this.cards.add(addCard);           // 'add' addCard to end of ArrayList
    }

        // Dealing Cards
    public void dealCard(Deck comingFrom){               // comingFrom == playingDeck
        this.cards.add(comingFrom.getCard(0));          // GET card    => playingDeck
        comingFrom.removeCard(0);                      // REMOVE card  => playingDeck
    }

    //  Remake full deck
    public void moveAllToDeck(Deck moveTo){         // moveTo == playingDeck
        int thisDeckSize = this.cards.size();
        // put cards into / moveTo deck
        for(int card = 0; card < thisDeckSize; card++){
            moveTo.addCard(this.getCard(card));
        }
        // enmpty out the deck
        for(int card2 = 0; card2 < thisDeckSize; card2++){
            this.removeCard(0);
        }
    }

    // Get side of ArrayList
    public int deckSize(){
        return this.cards.size();
    }

    // Return the total points of a hand
    public int getTotal(){
        int totalPoints = 0;
        boolean hasAce = false;
        // Getting total points (any ace by default == worth 1)
        for(int i = 0; i < cards.size(); i++){
            totalPoints += cards.get(i).getRANK_VALUE();
            // check to see if the card is ace
            if (cards.get(i).getRANK_STRING() == "Ace"){
                hasAce = true;
            }
            // make ace worth 11 if total points are less then or equal to 11
            if(hasAce && totalPoints <= 11){
                totalPoints += 10;      // add 10 more to the ace (totalpoints (ace == 1 included) <= 11 + 10 (to make ace == 11)
            }
        }
        return totalPoints;
    }

    public String toString(){                                         // returns the ArrayList of Card with toString from Cards (get.SuitText and get.rankString)
        String cardListOutPut = "";                                  // Starts out empty
        for(Card aCard : this.cards){                               // FOREACH: from 'Card' overgoing the ArrayList of 'cards'
            cardListOutPut += "\n" + "" + aCard.toString();        // cardListOutPut = CardListOutPut("") + (aCard + toString from Card)

        }
        return cardListOutPut;
    }


}
