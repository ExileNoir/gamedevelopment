package BlackjackGame;

public enum Rank {

    ACE(1, "Ace"),
    TWO(2, "Deuce"),
    THREE(3, "Three"),
    FOUR(4, "Four"),
    FIVE(5, "Five"),
    SIX(6, "Six"),
    SEVEN(7, "Seven"),
    EIGHT(8, "Eight"),
    NINE(9, "Nine"),
    TEN(10, "Ten"),
    JACK(10, "Jack"),
    QUEEN(10, "Queen"),
    KING(10, "King"),;


    // Instance Variable
    private final int RANK_VALUE;
    private final String RANK_STRING;

    // Constructor
    Rank(int RANK_VALUE, String RANK_STRING) {
        this.RANK_VALUE = RANK_VALUE;
        this.RANK_STRING = RANK_STRING;
    }

    // Method
        // Get  VALLUE (int)
    public int getRANK_VALUE() {
        return RANK_VALUE;
    }
        // Get  STRING (String)
    public String getRANK_STRING() {
        return RANK_STRING;
    }
}
