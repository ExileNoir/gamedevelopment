package BlackjackGame;

public class Card {

    // Instance Variables
    private Suit suit;
    private Rank rank;

    // Constructor
    public Card(Suit suit, Rank rank) {
        this.suit = suit;
        this.rank = rank;
    }

    // Methods
        // Getters
            // SUIT (String)
    public String getSUIT_TEXT() {                             // String != Suit (generate) to get SUIT_TEXT printed (!capitals)
        return suit.getSUIT_TEXT();                           // If Suit = we get capitals
    }

            // RANK (int)
    public int getRANK_VALUE(){                             // To get Rank -> value in int
        return rank.getRANK_VALUE();
    }
            // RANK (String)
    public String getRANK_STRING(){                       // to get Rank -> value in string (aces, king,...)
        return rank.getRANK_STRING();
    }


    // toString  // return this Suit str && return this Rank str (getters of Card)
    public String toString() {
        return this.getSUIT_TEXT() + " - " + this.getRANK_STRING();    // Return the Suit and Value (in String)
    }


}



