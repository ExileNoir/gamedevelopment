package BlackjackGame;

public enum Suit {

    HAERTS("Haerts"),
    SPADES("Spades"),
    DIAMONDS("Diamonds"),
    CLUBS("Clubs");


    // Instance Variable
    private final String SUIT_TEXT;

    // Constructor
    Suit(String SUIT_TEXT) {
        this.SUIT_TEXT = SUIT_TEXT;
    }

    // Methods
        // Get SUIT (string)
    public String getSUIT_TEXT() {
        return SUIT_TEXT;
    }


}
